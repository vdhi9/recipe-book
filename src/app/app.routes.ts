import {  Routes, RouterModule } from '@angular/router';
import { RecipesComponent } from 'app/recipes/recipes.component';
import { ShoppingListComponent } from 'app/shopping-list/shopping-list.component';
import { RECIPE_ROUTES } from './recipes/recipes.routes';

const APP_ROUTES: Routes = [
    { path:'', redirectTo:'/recipes', pathMatch:'full'},
    //  },
    { path:'recipes', component: RecipesComponent, children: RECIPE_ROUTES },
    { path:'shopping-list', component: ShoppingListComponent },
    
];

export const APP_ROUTES_PROVIDER=RouterModule.forRoot(APP_ROUTES);

//export const APP_ROUTES_PROVIDER=provideRouter(APP_ROUTES);

