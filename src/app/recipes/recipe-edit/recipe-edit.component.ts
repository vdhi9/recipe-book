import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router/src/router_state';
import { RecipeService } from 'app/recipes/recipe.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Recipe } from 'app/recipes/recipe';
import { FormArray, FormGroup, FormControl } from '@angular/forms/src/model';
import { Validators } from '@angular/forms/src/validators';
import { FormBuilder } from '@angular/forms/src/form_builder';
import { Router } from '@angular/router';
@Component({
  selector: 'rb-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styles: []
})
export class RecipeEditComponent implements OnInit, OnDestroy {
  recipeForm: FormGroup;
  private recipeIndex: number;
  private sub:any;  
  private recipe: Recipe;
  private isNew = true;
  constructor(private route: Router,private router: ActivatedRoute, private recipeservice: RecipeService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    
    this.sub = this.router.params.subscribe(
      (params: any)=> {
       
        if(params.hasOwnProperty('id')){
          this.isNew = false;
          this.recipeIndex = +params['id'];
          this.recipe = this.recipeservice.getRecipe(this.recipeIndex);
        }else{
          this.isNew = true;
          this.recipe = null;
        }
        this.initForm();
      }
    );
  }
private navigateBack(){
  this.route.navigate(['../']);
}
  onSubmit(){
    const newRecipe = this.recipeForm.value;
    if(this.isNew){
      this.recipeservice.addRecipe(newRecipe);
    }else{
      this.recipeservice.editRecipe(this.recipe, newRecipe);
    }
    this.navigateBack();
  }

  onCancel(){
    this.navigateBack();
  }

  onAddItem(name: string, amount: string){
    (<FormArray>this.recipeForm.controls['ingredients']).push(
      new FormGroup({
        name: new FormControl(name, Validators.required),
        amount: new FormControl(amount, [
          Validators.required,
          Validators.pattern("\\d+")
        ])
      })
    );
  }

  onRemoveItem(index: number){
    (<FormArray>this.recipeForm.controls['ingredients']).removeAt(index);
  }
  ngOnDestroy(){

    this.sub.unsubscribe();
  }

  private initForm(){
    debugger;
    let recipeName= '';
    let recipeImageUrl= '';
    let recipeContent= '';
    let recipeIngredient: FormArray = new FormArray([]);

    if(!this.isNew){
      for(let i =0; i<this.recipe.ingredients.length; i++){
        recipeIngredient.push(
          new FormGroup({
            name: new FormControl(this.recipe.ingredients[i].name, Validators.required),
            amount: new FormControl(this.recipe.ingredients[i].amount, [
              Validators.required, 
              Validators.pattern("\\d+")])
          })
        );
      }
      recipeName = this.recipe.name;
      recipeImageUrl = this.recipe.imagePath;
      recipeContent = this.recipe.description;

     
    }
    this.recipeForm = this.formBuilder.group({
      name: [recipeName, Validators.required],
      imagePath: [recipeImageUrl, Validators.required],
      description: [recipeContent, Validators.required],
      ingredients: recipeIngredient
    });
  }
}
