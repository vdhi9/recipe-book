import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[rbDropdown]'
})
export class DropdownDirective {

  @HostBinding('class.open') get opened(){ //opned property is read only due to get keyword. open from class.open is bootstrap internal class. it will only bind the property when class open is true.
    return this.isOpen;
  }

  @HostListener('click') open(){
    this.isOpen = true;
  }

  @HostListener('mouseleave') close(){
    this.isOpen = false;
  }
private isOpen = false;
}
