import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../recipe';
import { ShoppingListService } from "../../shopping-list/shopping-list.service";
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { RecipeService } from 'app/recipes/recipe.service';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
@Component({
  selector: 'rb-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit, OnDestroy {
  @Input() selectedRecipe: Recipe;
  private recipeIndex: number;
  private sub: any;

  constructor(private sls: ShoppingListService, 
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private recipeService: RecipeService
  ) {}

  ngOnInit() {
    this.sub = this.activatedRouter.params.subscribe(
      (params:any)=>{
        this.recipeIndex = params['id'];
        this.selectedRecipe = this.recipeService.getRecipe(this.recipeIndex);
      }
    );

  }
  onAddToShoppingList() {
    this.sls.addItems(this.selectedRecipe.ingredients);
  }

  onEdit(){
    this.router.navigate(['/recipes', 'edit', this.recipeIndex]);
  }

  onDelete(){
    this.recipeService.deleteRecipe(this.selectedRecipe);
    this.router.navigate(['/recipes']);
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }
}
